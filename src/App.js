import React, {Component} from 'react';
import './App.css';
import 'survey-react/survey.css';
import * as survey from 'survey-react';

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      
    }
    this.onCompleteComponent = this.onCompleteComponent.bind(this);
  }
  onCompleteComponent = () => {
    this.setState ({
      isCompleted: true
    })
  }

  render(){
    var defaultThemeColors = survey
    .StylesManager
    .ThemeColors["default"];
    defaultThemeColors["$main-color"] = "#e0084b";
    defaultThemeColors["$main-hover-color"] = "#b3063c";
    defaultThemeColors["$text-color"] = "#4a4a4a";
    defaultThemeColors["$header-color"] = "#e0084b";

    defaultThemeColors["$header-background-color"] = "#4a4a4a";
    defaultThemeColors["$body-container-background-color"] = "#f8f8f8";

    survey
    .StylesManager
    .applyTheme();

    var json = {
      title: "Covid-19 self-checker",
      triggers: [
        {
            type: "complete",
            expression: "{illness} = 'No'"
        }
        , {
            type: "complete",
            expression: "{location} = 'Outside the US'"
        }
    ],
      pages: [
          {
              
              questions: [
                  {
                     title: "Are you ill, or caring for someone who is ill?",
                      type: "radiogroup",
                      name: "illness",
                      hasOther: false,
                      isRequired: true,
                      choices: ["Yes", "No"]
                  }
              ]
          }
          , {
              title: "Where are you (they) located?",
              questions: [
                  {
                      type: "radiogroup",
                      name: "location",
                      title: "Please select from the list",
                      colCount: 1,
                      isRequired: true,
                      choices: [
                          "United States",
                          "Outside the US"
                      ]
                  }
              ]
          },{
            title: "please select your location",
            questions: [
              {
                type: "dropdown",
                name: "reg_country",
                title: "Select the country...",
                isRequired: true,
                choicesByUrl: {
                    url: "https://restcountries.eu/rest/v2/region/Americas",
                    valueName: "name"
                }
              }
            ]
        }, {
          title: "Are you answering for yourself or someone else?",
          questions: [
              {
                  type: "radiogroup",
                  name: "whom",
                  title: "Please select from the list",
                  colCount: 1,
                  isRequired: true,
                  choices: [
                      "Myself",
                      "SomeoneElse"
                  ]
              }
          ]
      },
      {
        title: "What is your age?",
        questions: [
            {
                type: "radiogroup",
                name: "age",
                title: "Please select from the list",
                colCount: 3,
                isRequired: true,
                choices: [
                  '2 - 9', '5 - 9', '10 - 19',
                  '20 - 29','30 - 39','40 - 50',
                  'above 50'
                  ]
              }
             ]
         },
         {
          title: "What is your gender?",
          questions: [
              {
                  type: "radiogroup",
                  name: "gender",
                  title: "Please select from the list",
                  colCount: 1,
                  isRequired: true,
                  choices: [
                      "Male",
                      "Female"
                  ]
              }
          ]
      },
      {
        title: "Do you have any of the following life-threatening symptoms?",
        questions: [
            {
                type: "checkbox",
                name: "threatening",
                title: "Please select from the list",
                colCount: 1,
                isRequired: true,
                choices: [
                  "Not experiencing any life-threatening symptoms",

                  "Gasping for air or cannot talk without catching your breath (extremely difficult breathing)",
                  
                  "Blue-colored lips or face",
                  
                  "Severe and constant pain or pressure in the chest",
                  
                  "Severe and constant dizziness or lightheadedness",
                  
                  "Acting confused (new or worsening)",
                  
                  "Unconscious or very difficult to wake up",
                  
                  "Slurred speech (new or worsening)",
                  
                  "New seizure or seizures that won’t stop"
                ]
            }
        ]
    },{
              title: "Call 911 - You may be having a medical emergency.",
              questions: [
                  {
                      type: "text",
                      name: "name",
                      title: "Your Name:"
                  }, {
                      type: "text",
                      name: "email",
                      title: "Your e-mail"
                  }
              ]
          }
      ],
      completedHtml: "<p><h4>Call 911 - You may be having a medical emergency.</p></h4>"

  };
  var model = new survey.Model(json);
  // window.survey = new survey.Model(json);

  var surveyRender = (
    <survey.SurveyWindow model={model} 
    showCompletedPage = {false}
    onComplete = {this.onCompleteComponent}
    />
    // <survey.Survey
    // model={model}
    // showCompletedPage = {false}
    // onComplete = {this.onCompleteComponent}
    // />
  );

  // var onSurveyCompletion = this.state.isCompleted ?(

  //   <div>Thanks for completing the survey</div>
  // ) : null;
    return (
      <div className="App">
        {surveyRender} 
        {/* {onSurveyCompletion} */}
      </div>
    );
  }
  }


export default App;
